const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController")
const auth = require("../auth")

router.post("/add", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin){

		productController.addProduct(data).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false)
	}

	
});


router.get ("/", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});



router.get("/viewActive", (req, res) => {
	productController.getActiveProduct().then(resultFromController => res.send(resultFromController));
});

router.get("/:productId", (req, res) => {
	console.log(req.params.productId)
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


router.put("/:productId", auth.verify, (req, res) => {
	const data = auth.decode(req.headers.authorization);
	if (data.isAdmin) {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}
});


router.put("/:productId/archive", auth.verify, (req, res) => {
	productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});




module.exports = router;