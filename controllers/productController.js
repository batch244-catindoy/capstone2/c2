const Product = require("../DataModel/Product");


module.exports.addProduct = (data) => {

	let newProduct = new Product ({
		name: data.product.name,
		description: data.product.description,
		price: data.product.price
	});
	return newProduct.save().then((product, error) => {
		if(error){
			return false
		}else {
			return true;
		}
	})
};


module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
};



module.exports.getActiveProduct = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
};


module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
};


module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		totalAmount: reqBody.totalAmount
	};
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course, error) => 
	{
		if (error) {
			return false;
		} else {
			return true
		}
	})
};


module.exports.archiveProduct = (reqParams, reqBody) => {
	let updateActiveField = {
		isActive : reqBody.isActive
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((course, error) => {

		if(error){
			return false;
		}else {
			return true;
		}
	})
};


