const bcrypt = require("bcrypt");
const auth = require("../auth")
const User = require("../DataModel/User");
const Product = require("../DataModel/Product")

module.exports.registerUser = (reqBody) => {

	
	let newUser = new User({
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) => {	
		if (error) {

			return false;
		} else {

			return true;
		}
	})
}

module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {
		if (result == null) {
			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect) {
				return {access : auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}


module.exports.checkout = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.orders.push({productId : data.productId});
		return user.save().then((user, error) => {
			if (error){
				return false;
			}else {
				return true;
			}
		})
	});

	let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.orders.push({userId : data.userId})
		return product.save().then((product, error) => {
			if (error) {
				return false;
			}else {
				return true;
			}
		})
	});

	if (isUserUpdated && isProductUpdated) {
		return true;
	} else {
		return false;
	}
}


module.exports.getProfile = (reqBody) => {

	return User.findById(reqBody.userId).then(result => {
		result.password = "";
		return result;

	});

};

module.exports.orders = () => {
	
	return User.find({isAdmin:false}).then((user) => {

		let orders = []
		user.forEach(user => {
			orders.push(user.orders)
		})

		return orders;
	})
};



module.exports.setAdmin = (reqParams, reqBody) => {
	let updateToActiveAdmin = {
		isActive : reqBody.isActive
	};

	return User.findByIdAndUpdate(reqParams.userId, updateToActiveAdmin).then((user, error) => {

		if(error){
			return false;
		}else {
			return true;
		}
	})
};