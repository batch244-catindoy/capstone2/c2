const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: [true, "Description is required."]
		},
		description: {
			type: String
		},
		price: {
			type: Number
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
		},
		orders: [
			{
				orderId: {
					type: String,
				},
			}
		]
	}
)
module.exports = mongoose.model("Product", productSchema)