const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController")
const auth = require("../auth");

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send (resultFromController))
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send (resultFromController))
});



router.post("/checkout", auth.verify, (req, res) => {

	let data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		productId : req.body.productId
	}

	if (data.isAdmin == false) {
		userController.checkout(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}
});


router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});


router.get ("/orders", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		productId : req.body.productId
	}
	if (!data.isAdmin) {
		userController.orders(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}
});


router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	userController.setAdmin(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

module.exports = router;